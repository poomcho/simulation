pkill -9 mavlink-router

kill -9 $(lsof -t -i:14550)

kill -9 $(lsof -t -i:14585)

kill -9 $(lsof -t -i:14590)

mavlink-routerd -e 127.0.0.1:14585 -e 127.0.0.1:14590 127.0.0.1:14540 &

cd 

pwd

./.local/lib/python3.8/site-packages/mavsdk/bin/mavsdk_server -p 50040 udp://:14585 &

./.local/lib/python3.8/site-packages/mavsdk/bin/mavsdk_server -p 50041 udp://:14590 &

cd Desktop/PX4-Autopilot

pwd

export PX4_HOME_LAT=13.562225

export PX4_HOME_LON=100.800074

HEADLESS=1 make px4_sitl gazebo